// facebook
// App ID: 337116343057831
// App Secret: 478ad0bddbdff37d058b858f83e5904e
//
// lastfm
// API Key: eabd68efc31cdb4c26ff4618b177e726
// Secret: is 7ac386971291715d1f3f776b25c60250
//
// meetup
// API Key: 64b1c264713a555f4f3214125d1e14
//
// eventful
// API key: fpJX8SW722pcXRMp
// http://api.eventful.com/json/events/search?app_key=fpJX8SW722pcXRMp&where=59.32893000000001,18.06491&within=50
//
// tickster
// API key: 5fa7c6675abdd345
// http://www.tickster.com/sv/api/0.2/events/upcoming?key=5fa7c6675abdd345
//
( function ()
  {
    "use strict";
    function allowCrossDomain ( req, res, next )
      {
        res
          .header(
            "Access-Control-Allow-Origin",
            "*"
          )
          .header(
            "Access-Control-Allow-Methods",
            "GET,OPTIONS"
          )
          .header(
            "Content-Type",
            "application/json; charset=utf-8"
          )
          .header(
            "Access-Control-Allow-Headers",
            "X-Requested-With,Content-Type,Origin,Accept,Authorization"
          );
        next();
      }
    function getRequest ( options, callback )
      {
        var respose, req;
        if ( !options ) callback( false );
        options.headers =
          {
            "Accept-Charset": "utf-8"
          };
        req = http.request(
          options,
          function ( res )
            {
              var data;
              data = "";
              res.on(
                "data",
                function ( chunk )
                  {
                    data += chunk.toString();
                  }
              );
              res.on(
                "end",
                function ()
                  {
                    try {
                      if ( data ) {
                        callback( JSON.parse( data ) );
                      } else {
                        callback( null );
                      }
                    } catch ( e ) {
                      console.error( e );
                      callback( null );
                    }
                  }
              );
            }
        );
        req.on(
          "error",
          function ( error )
            {
              console.error( error );
              callback( null );
            }
          );
        req.end();
      }
    function Normalized ()
      {
        return (
          {
            description: "",
            latitude: "",
            location: "",
            longitude: "",
            name: "",
            start: "",
            url: "",
            categories: []
          }
        );
      }
    function newFQLQuery ( opt )
      {
        var query, longitude, latitude, offset;
        longitude = opt.lon ? opt.lon : pos.lon;
        latitude = opt.lat ? opt.lat : pos.lat;
        offset = opt.off ? opt.off : pos.off;
        offset = offset * 100;
        query = "SELECT name,location,description,start_time,end_time,venue FROM event WHERE eid IN (SELECT eid FROM event_member WHERE uid IN (SELECT page_id FROM place WHERE distance(latitude, longitude, '" + latitude + "', '" + longitude + "') < " + offset + "))";
        return query;
      }
    function newLASTFMQuery ( opt )
      {
        var options, longitude, latitude, offset;
        longitude = opt.lon ? opt.lon : pos.lon;
        latitude = opt.lat ? opt.lat : pos.lat;
        offset = opt.off ? opt.off : pos.off;
        offset = offset * 10;
        options =
          {
            host: "ws.audioscrobbler.com",
            path: "/2.0/?method=geo.getevents&lat=" + latitude + "&long=" + longitude + "&distance=" + offset + "&api_key=eabd68efc31cdb4c26ff4618b177e726&format=json"
          };
        return options;
      }
    function newGOOGLEQuery ( opt )
      {
        var options, longitude, latitude;
        longitude = opt.lon ? opt.lon : pos.lon;
        latitude = opt.lat ? opt.lat : pos.lat;
        options =
          {
            host: "maps.googleapis.com",
            path: "/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&sensor=false"
          };
        return options;
      }
    function newGOOGLESEARCHQuery ( opt )
      {
        var options, term, replaceREG;
        replaceREG = /\s|\t|\n|\r/g;
        term = opt ? opt : "";
        term = term.replace( replaceREG, "+" );
        options =
          {
            host: "maps.googleapis.com",
            path: "/maps/api/geocode/json?address=" + term + "&sensor=false"
          };
        return options;
      }
    function newEVENTFULQuery ( opt )
      {
        var options, longitude, latitude, offset;
        longitude = opt.lon ? opt.lon : pos.lon;
        latitude = opt.lat ? opt.lat : pos.lat;
        offset = opt.off ? opt.off : pos.off;
        offset = offset * 10;
        options =
          {
            host: "api.eventful.com",
            path: "/json/events/search?app_key=fpJX8SW722pcXRMp&where=" + latitude + "," + longitude + "&within=" + offset
          };
        return options;
      }
    function getEventsFacebook ( opt, callback )
      {
        var query;
        query = newFQLQuery( opt );
        console.log( "newFQLQuery", "query", query );
        graph.api(
          {
            method: "fql.query",
            query: query
          },
          function ( data )
            {
              data = ( data && data.length ) ? data : [];
              data = data.map(
                function ( event )
                  {
                    var newData = new Normalized();
                    newData.description = event.description ? event.description : "";
                    newData.latitude = ( event.venue && event.venue.latitude ) ? event.venue.latitude : "";
                    newData.location = ( event.location ) ? event.location : "";
                    newData.longitude = ( event.venue && event.venue.longitude ) ? event.venue.longitude : "";
                    newData.name = event.name ? event.name : "";
                    newData.start = event.start_time ? new Date( event.start_time ) : "";
                    newData.url = event.eid ? "http://facebook.com/event/" + event.eid : "";
                    newData.categories = [ "facebook" ];
                    return newData;
                  }
              );
              return callback( { data: data } );
            }
          );
      }
    function getEventsLastFM ( opt, callback )
      {
        var options;
        options = newLASTFMQuery( opt );
        console.log( "newLASTFMQuery", "options", options );
        getRequest(
          options,
          function ( data )
            {
              if ( !data ) return callback( [] );
              data = ( data && data.events && data.events.event && data.events.event.length ) ? data.events.event : [];
              data = data.map(
                function ( event )
                  {
                    var newData = new Normalized();
                    newData.description = event.description ? event.description : "";
                    newData.latitude = ( event.venue && event.venue.location && event.venue.location["geo:point"] && event.venue.location[ "geo:point" ][ "geo:lat" ] ) ? event.venue.location[ "geo:point" ][ "geo:lat" ] :"";
                    newData.location = ( event.venue && event.venue.name ) ? event.venue.name : "";
                    newData.longitude = ( event.venue && event.venue.location && event.venue.location["geo:point"] && event.venue.location[ "geo:point" ][ "geo:long" ] ) ? event.venue.location[ "geo:point" ][ "geo:long" ] : "";
                    newData.name = event.title ? event.title : "";
                    newData.start = event.startDate ? new Date( event.startDate ) : "";
                    newData.url = event.website ? event.website : ( event.url ) ? event.url : "";
                    newData.categories = [ "lastfm" ];
                    return newData;
                  }
              );
              return callback( { data: data } );
            }
          );
      }
    function getEventsEventful ( opt, callback )
      {
        var options;
        options = newEVENTFULQuery ( opt );
        console.log( "newEVENTFULQuery", "options", options );
        getRequest(
          options,
          function (  data )
            {
              if ( !data ) return callback( [] );
              data = ( data && data.events && data.events.event && data.events.event.length ) ? data.events.event : [];
              //console.log( data[ 0 ] );
              data = data.map(
                function ( event )
                  {
                    var newData = new Normalized();
                    newData.description = event.description ? event.description : "";
                    newData.latitude = event.latitude ? event.latitude : "";
                    newData.location = ( event.venue_name ) ? event.venue_name : "";
                    newData.longitude = event.longitude ? event.longitude : "";
                    newData.name = event.title ? event.title : "";
                    newData.start = event.start_time ? new Date( event.start_time ) : "";
                    newData.url = event.venue_url ? event.venue_url : ( event.url ) ? event.url : "";
                    newData.categories = [ "eventful" ];
                    return newData;
                  }
              );
              return callback( { data: data } );
            }
          );
      }
    function getUserLocation ( opt, callback )
      {
        var options;
        options = newGOOGLEQuery( opt );
        getRequest(
          options,
          function ( data )
            {
              data = ( ( !data ) ? "" : ( data && !data.results || data && data.results && !data.results.length ) ? "" : ( data && data.results && data.results[ 2 ].formatted_address ) ? data.results[ 2 ].formatted_address : ( data.results[ 1 ].formatted_address ) ? data.results[ 1 ].formatted_address : ( data.results[ 0 ].formatted_address ) ? data.results[ 0 ].formatted_address : "" );
              callback( { from: data } );
            }
        );
      }
    function getUserLocationBySearch ( opt, callback )
      {
        var options;
        options = newGOOGLESEARCHQuery( opt );
        getRequest(
          options,
          function ( data )
            {
              data = ( data && data.results && data.results.length ) ? data.results[ 0 ] : {};
              data = ( data.geometry ) ? data.geometry : {};
              data = ( data.location && data.location.lat && data.location.lng ) ? { longitude: data.location.lng, latitude: data.location.lat } : false;
              callback( data );
            }
        );
      }
    function clearSaved ()
      {
        var i, now, selected, b;
        i = saved.length;
        if ( !i ) return;
        now = new Date();
        while ( i-- )
          {
            selected = saved[ i ];
            if ( selected && now - selected.time > 30 * 1000 )
              {
                saved.splice( i, 1 );
              }
          }
      }
    function compare ( a, b ) // returns the array, ASC
      {
        var c, d;
        c = a.start;
        d = b.start;
        if ( c > d )
           return 1;
        if ( c < d )
          return -1;
        return 0;
      }
    function GetLocationBySearch ( options, res )
      {
        getUserLocationBySearch(
          options.term,
          function ( data )
            {
              if ( !data || !data.longitude || !data.latitude )
                {
                  return res.json(
                    {
                      categories: [],
                      data: [],
                      from: "location not found"
                    }
                  );
                }
              data.category = options.category ? options.category : "";
              new HandleUserRequest( data, res );
            }
        );
      }
    function HandleUserRequest ( query, res )
      {
        function asyncArray( item, done )
          {
            item(
              function ( cb )
                {
                  var union, results;
                  if ( cb && cb.from ) from = cb.from;
                  outer.push( ( cb && cb.data ) ? cb.data : [] );
                  if ( outer.length >= Services.length )
                    {
                      union = _.flatten( outer ).sort( compare );
                      results =
                        {
                          categories: CategoriesName,
                          data: union,
                          from:
                            {
                              longitude: options.lon,
                              latitude: options.lat,
                              name: from ? from : "earth"
                            }
                        };
                      return done( results );
                    }
                }
            );
          }
        function innerQuery ( func )
          {
            return (
              function ( callback )
                {
                  func.get(
                    options,
                    function ( data )
                      {
                        return callback( data );
                      }
                  );
                }
            );
          }
        var options, area,
            i, _saved, from,
            _lo, _la,
            _useSaved, _useSavedIteration,
            callbacks, outer;
        if ( query.longitude === undefined || query.latitude === undefined )
          {
            return res.json( query );
          }
        from = "";
        area = 0.001;
        i = saved.length;
        if ( !!i )
          {
            _useSaved = false;
            while ( i-- )
              {
                _saved = saved[ i ];
                if ( !_saved || _saved.lon === undefined || _saved.lat  === undefined ) continue;
                _lo = Math.abs( _saved.lon - query.longitude );
                _la = Math.abs( _saved.lat - query.latitude );
                if ( _la > area && _lo > area ) continue;
                _useSaved = true;
                _useSavedIteration = i;
                break;
              }
            if ( _useSaved )
              {
                if ( query.category && query.category.length )
                  {
                    return getFromCategories(
                      query.category,
                      saved[ _useSavedIteration ].res,
                      res
                    );
                  }
                else
                  {
                    return res.send( saved[ _useSavedIteration ].res );
                  }
              }
          }
        options =
          {
            lon: query.longitude,
            lat: query.latitude,
            cat: query.category,
            off: query.offset ? query.offset : pos.off
          };
        options.off = parseInt( options.off, 10 ) % 50;
        options.off = options.off < 1 ? 1 : options.off;
        options.off = options.off.toString();
        callbacks = [];
        outer = [];
        Services.map(
          function ( service )
            {
              if ( !service || !service.get) return;
              callbacks.push( innerQuery( service ) );
            }
        );
        async.forEach(
          callbacks,
          asyncArray,
          function( result )
            {
              setCategories(
                result,
                options.cat,
                function ( categorised )
                  {
                    var newSaved;
                    newSaved =
                      {
                        lat: query.latitude,
                        lon: query.longitude,
                        time: new Date(),
                        res: categorised
                      };
                    saved.push( newSaved );
                    if ( options.cat && options.cat.length )
                      {
                        getFromCategories( options.cat, categorised, res );
                      }
                    else
                      {
                        res.send( categorised );
                      }
                  }
              );
            }
        );
      }
    function getFromCategories ( cat, categorised, res )
      {
        var newdata, reg, data;
        newdata = [];
        data = categorised.data.slice();
        cat = cat.toLowerCase().split( "," );
        reg = new RegExp( "\\b" + cat.join("\\b|\\b") + "\\b" );
        data.map(
          function ( inner )
            {
              var category, i, _cat, match;
              category = inner.categories;
              i = category.length;
              match = false;
              while ( i-- )
                {
                  _cat = category[ i ];
                  if ( _cat.match( reg ) )
                    {
                      match = true;
                      break;
                    }
                }
              if ( !!match ) newdata.push( inner );
            }
        );
        if ( newdata.length )
          {
            categorised.data = newdata;
          }
        res.send( categorised );
      }
    function setCategories ( result, cat, callback )
      {
        var i, j, data, inner,
            category, reg, matching,
            name;
        data = result.data;
        i = data.length;
        while ( i-- )
          {
            inner = data[ i ];
            if ( !inner ) continue;
            j = Categories.length;
            name = data[ i ].categories;
            if ( !name ) continue;
            while ( j-- )
              {
                category = Categories[ j ];
                if ( !category ) continue;
                matching = category.matching.join( "|" );
                reg = new RegExp( "(" + matching + ")", "gi" );
                if ( !!inner.description.match( reg ) || !!inner.name.match( reg ) )
                  {
                    data[ i ].categories.push( category.name.toLowerCase() );
                  }
              }
          }
        return callback( result );
      }

    var Graph, graph, config,
        http, fs, file,
        pos, Express, app, Services,
        saved, async, _, Categories,
        CategoriesName, _i, _cat;
    Graph = require( "facebook-sdk" );
    Express = require( "express" );
    async  = require( "async" );
    http = require( "http" );
    fs = require( "fs" );
    _ = require( "underscore" );
    app = new Express();
    saved = [];
    config = // facebook-config
      {
        appId: "337116343057831",
        secret: "478ad0bddbdff37d058b858f83e5904e"
      };
    pos = // standard position.
      {
        lat: "0",
        lon: "0",
        off: "1" // 50000
      };
    Services =
      [
        { get: getEventsLastFM },
        { get: getEventsFacebook },
        { get: getEventsEventful },
        { get: getUserLocation }
      ];
    file = fs.readFileSync( __dirname + "/matching.json" );
    file = JSON.parse( file );
    Categories = file && file.Categories ? file.Categories : [];
    CategoriesName = [];
    _i = Categories.length;
    while ( _i-- )
    {
      _cat = Categories[ _i ].name;
      if ( !_cat ) continue;
      CategoriesName.push( _cat.toLowerCase() );
    }
    _i = file && file.Services ? file.Services.length : 0;
    while ( _i-- )
    {
      _cat = file.Services[ _i ];
      if ( !_cat ) continue;
      CategoriesName.push( _cat.toLowerCase() );
    }
    CategoriesName.sort();
    _cat = null;
    graph = new Graph.Facebook( config );
    app
      .configure(
        function ()
          {
            app
              .use( allowCrossDomain )
              .use( Express.bodyParser() )
              .use( Express.methodOverride() )
              .use( app.router );
          }
        )
      .listen(
        5001
      );
    app
      .get(
        "/",
        function ( req, res )
          {
            var query;
            query = req.query;
            new HandleUserRequest( query, res );
          }
      )
      .options(
        "*",
        function ( req, res )
          {
            res.json( {} );
          }
      )
      .get(
        "/search/:term",
        function ( req, res )
          {
            var query, options, term, matchREG;
            query = req.query;
            matchREG = /^([\wåäö\s]*)((?:\,){1}(?:[\wåäö\s]*)){0,}/ig;
            term = req.params.term;
            term = term.toLowerCase();
            term = term.replace(
              matchREG,
              function()
                {
                  var arg, ret;
                  arg = arguments;
                  ret = arg[ 2 ] ? arg[ 1 ] + arg[ 2 ] : arg[ 1 ] + ",sweden";
                  return ret;
                }
            );
            options =
              {
                term: term,
                category: query.category ? query.category : "",
                offset: query.offset ? query.offset : "10"
              };
            new GetLocationBySearch( options, res );
          }
      );
    setInterval(
      clearSaved,
      30 * 1000
    );
  }
)();

